package stockmarket;
public class SoyStockHolder implements Observer{
    private double SoyPrice, ValueBuy, ValueSell;
    //Static variable used as a counter
    private static int obsIDTracker = 0;
    //Observer id
    private int obsID;
    //Will hold reference to the Market
    private Subject market;
    public SoyStockHolder(Subject market) {
        this.market = market;
        //Assign an Observer ID and increment static counter
        this.obsID = ++obsIDTracker;
        //Message of new Observer
        System.out.println("New Dollar Observer ID: "+this.obsID);
        //Adding observer to subject arraylist
        this.market.attach(this);
    }
    public void setValueBuy(double ValueBuy) {
        this.ValueBuy = ValueBuy;
    }
    public void setValueSell(double ValueSell) {
        this.ValueSell = ValueSell;
    }
    //Called to update all observers
    @Override
    public void update(double Dollar, double Gold, double Soy) {
        this.SoyPrice = Soy;
        BuyAndSell();
    }
    private void BuyAndSell() {
        System.out.println("Soy Observer ID: "+this.obsID+" Soy Price: "+this.SoyPrice);
        if(this.SoyPrice > ValueSell){
            System.out.println("Selling Soy!\n");
        }else if(this.SoyPrice <= ValueBuy){
            System.out.println("Buy Soy!\n");
        }else{
            System.out.println("Holding Position!\n");
        }
    }
}