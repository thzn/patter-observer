package stockmarket;
public class main {
    public static void main(String[] args) {
        StockExchange StockPVH = new StockExchange();
        //Creating Observers to my stock
        DollarStockHolder obs1 = new DollarStockHolder(StockPVH);
        GoldStockHolder obs2 = new GoldStockHolder(StockPVH);
        SoyStockHolder obs3 = new SoyStockHolder(StockPVH);
        
        obs1.setValueBuy(3.50);
        obs1.setValueSell(4.00);
        obs2.setValueSell(3.20);
        obs2.setValueSell(3.80);
        obs3.setValueBuy(70);
        obs3.setValueSell(100);
        
        //Changing Subject States
        StockPVH.setPrices(4.10, 100, 75);
        StockPVH.setPrices(3.80, 115, 60);
        StockPVH.setPrices(3.50, 135, 120);
        
    }
}
